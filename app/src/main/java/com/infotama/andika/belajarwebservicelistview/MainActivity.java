package com.infotama.andika.belajarwebservicelistview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.infotama.andika.belajarwebservicelistview.Models.Listbuku;
import com.infotama.andika.belajarwebservicelistview.Models.Models;
import com.infotama.andika.belajarwebservicelistview.Service.RestApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static String ROOT_URL = "http://103.229.74.80";
    public static String ID_BUKU = "id_buku";
    public static String JUDUL_BUKU = "judul_buku";
    public static String PENERBIT_BUKU = "penerbit_buku";
    public static String TAHUN_BUKU = "tahun_buku";
    public static String PENGARANG_BUKU = "pengarang_buku";

    private ListView listView;
    private List<Listbuku> books;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
           @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
               Intent inten = new Intent(MainActivity.this, DetailBukuActivity.class);
               Listbuku listBuku = books.get(position);
               inten.putExtra(JUDUL_BUKU, listBuku.getJudul());
               inten.putExtra(PENERBIT_BUKU,listBuku.getPenerbit());
               inten.putExtra(TAHUN_BUKU, listBuku.getTahun());
               inten.putExtra(PENGARANG_BUKU, listBuku.getPengarang());

               startActivity(inten);
           }
        });

        getBuku();

    }

    private void getBuku(){
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching data","Please wait..",false,false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RestApi service = retrofit.create(RestApi.class);
        Call <Models> call = service.loadListBuku();

        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                loading.dismiss();
                List<Listbuku> buku = response.body().getListbuku();

                books = buku;

                showList();
            }

            @Override
            public void onFailure(Call<Models> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(MainActivity.this,"Failure",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showList(){
        String[] items = new String[books.size()];

        for(int i=0; i <books.size();i++){
            items[i] = books.get(i).getJudul();
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.buku_row,R.id.textView,items);

        listView.setAdapter(adapter);
    }
}
