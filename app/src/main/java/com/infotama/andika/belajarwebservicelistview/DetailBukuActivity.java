package com.infotama.andika.belajarwebservicelistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DetailBukuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_buku);

        TextView textJudul = (TextView) findViewById(R.id.text_judul);
        TextView textTahun = (TextView) findViewById(R.id.text_tahun);
        TextView textPengarang = (TextView) findViewById(R.id.text_pengaran);
        TextView textPenerbit = (TextView)findViewById(R.id.text_penerbit);

        Intent intent = getIntent();

        textJudul.setText("JUDUL : " + intent.getStringExtra(MainActivity.JUDUL_BUKU));
        textTahun.setText("TAHUN : " + intent.getStringExtra(MainActivity.TAHUN_BUKU));
        textPengarang.setText("Pengarang :" + intent.getStringExtra(MainActivity.PENGARANG_BUKU));
        textPenerbit.setText("Penerbit :" + intent.getStringExtra(MainActivity.PENERBIT_BUKU));


    }
}
