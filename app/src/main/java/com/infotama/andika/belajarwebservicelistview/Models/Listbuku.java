
package com.infotama.andika.belajarwebservicelistview.Models;

import java.util.HashMap;
import java.util.Map;


public class Listbuku {

    private Integer id;
    private String judul;
    private String penerbit;
    private String tahun;
    private String pengarang;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The judul
     */
    public String getJudul() {
        return judul;
    }

    /**
     * 
     * @param judul
     *     The judul
     */
    public void setJudul(String judul) {
        this.judul = judul;
    }

    /**
     * 
     * @return
     *     The penerbit
     */
    public String getPenerbit() {
        return penerbit;
    }

    /**
     * 
     * @param penerbit
     *     The penerbit
     */
    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    /**
     * 
     * @return
     *     The tahun
     */
    public String getTahun() {
        return tahun;
    }

    /**
     * 
     * @param tahun
     *     The tahun
     */
    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    /**
     * 
     * @return
     *     The pengarang
     */
    public String getPengarang() {
        return pengarang;
    }

    /**
     * 
     * @param pengarang
     *     The pengarang
     */
    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
