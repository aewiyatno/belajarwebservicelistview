package com.infotama.andika.belajarwebservicelistview.Service;

import com.infotama.andika.belajarwebservicelistview.Models.Models;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Andika on 09/09/2016.
 */
public interface RestApi {
    @GET("listbuku")
    Call<Models> loadListBuku();
}
